package org.jbb.cloud.swan.healthcheck.indicator;

import static org.assertj.core.api.Assertions.assertThat;

import org.jbb.cloud.swan.healthcheck.HealthDetail;
import org.jbb.cloud.swan.healthcheck.HealthStatus;
import org.junit.Test;

public class SwanFrameworkHealthIndicatorTest {

  @Test
  public void shouldReturnSwanVersionFromClasspathFile() {
    // when
    SwanFrameworkHealthIndicator swanFrameworkHealthIndicator = new SwanFrameworkHealthIndicator();
    HealthDetail healthDetail = swanFrameworkHealthIndicator.getHealthDetail();

    // then
    assertThat(healthDetail.getName()).isEqualTo("swan-framework-status");
    assertThat(healthDetail.getStatus()).isEqualTo(HealthStatus.NOT_APPLICABLE);
    assertThat(healthDetail.getAdditionalInfo())
        .isEqualTo("Running Swan Framework with version: DYNAMIC-SNAPSHOT");

  }

}