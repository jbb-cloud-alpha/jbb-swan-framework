package org.jbb.cloud.swan.healthcheck;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class HealthDetail {

  private String name;
  private HealthStatus status;
  private String additionalInfo;

}
