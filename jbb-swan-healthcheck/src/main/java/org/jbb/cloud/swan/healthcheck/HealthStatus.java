package org.jbb.cloud.swan.healthcheck;


public enum HealthStatus {

  HEALTHY, UNHEALTHY, NOT_APPLICABLE

}
