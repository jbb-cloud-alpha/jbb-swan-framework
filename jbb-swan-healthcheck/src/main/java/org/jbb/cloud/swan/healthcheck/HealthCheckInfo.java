package org.jbb.cloud.swan.healthcheck;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class HealthCheckInfo {

  private String appId;
  private HealthStatus status;
  private LocalDateTime startupDateTime;
  private LocalDateTime lastCheckDateTime;
  private Set<HealthDetail> details = new HashSet<>();

}
