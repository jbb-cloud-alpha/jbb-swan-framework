package org.jbb.cloud.swan.healthcheck;

public interface HealthIndicator {

  HealthDetail getHealthDetail();

}
