package org.jbb.cloud.swan.healthcheck.indicator;

import java.io.IOException;
import java.nio.charset.Charset;
import org.apache.commons.io.IOUtils;
import org.jbb.cloud.swan.healthcheck.HealthDetail;
import org.jbb.cloud.swan.healthcheck.HealthIndicator;
import org.jbb.cloud.swan.healthcheck.HealthStatus;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

@Component
public class SwanFrameworkHealthIndicator implements HealthIndicator {

  private final String swanVersion;

  public SwanFrameworkHealthIndicator() {
    try {
      ClassPathResource classPathResource = new ClassPathResource("swan.version");
      swanVersion = IOUtils.readLines(classPathResource.getInputStream(), Charset.forName("UTF-8"))
          .get(0);
    } catch (IOException e) {
      throw new IllegalStateException(e);
    }
  }

  @Override
  public HealthDetail getHealthDetail() {
    return HealthDetail.builder()
        .name("swan-framework-status")
        .additionalInfo("Running Swan Framework with version: " + swanVersion)
        .status(HealthStatus.NOT_APPLICABLE)
        .build();
  }

}
