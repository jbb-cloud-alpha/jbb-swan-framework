package org.jbb.cloud.swan.healthcheck;

import org.jbb.cloud.swan.healthcheck.indicator.SwanFrameworkHealthIndicator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HealthCheckConfiguration {

  @Bean
  public SwanFrameworkHealthIndicator swanFrameworkHealthIndicator() {
    return new SwanFrameworkHealthIndicator();
  }

}
