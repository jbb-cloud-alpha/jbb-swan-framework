package org.jbb.cloud.swan.autoconfigure.healthcheck;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.jbb.cloud.swan.healthcheck.HealthCheckInfo;
import org.jbb.cloud.swan.healthcheck.HealthDetail;
import org.jbb.cloud.swan.healthcheck.HealthIndicator;
import org.jbb.cloud.swan.healthcheck.HealthStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class HealthCheckServiceImpl implements HealthCheckService {

  private final String appId;
  private final LocalDateTime startupDateTime;
  private final List<? extends HealthIndicator> healthIndicators;

  private LocalDateTime lastCheckDateTime;
  private Set<HealthDetail> currentHealthDetails;

  @Autowired
  public HealthCheckServiceImpl(String appId,
      List<? extends HealthIndicator> healthIndicators) {
    this.appId = appId;
    this.startupDateTime = LocalDateTime.now();
    this.healthIndicators = healthIndicators;
  }

  @Override
  public HealthCheckInfo getHealthCheckInfo() {
    return HealthCheckInfo.builder()
        .appId(appId)
        .details(currentHealthDetails)
        .status(resolveStatus(currentHealthDetails))
        .startupDateTime(startupDateTime)
        .lastCheckDateTime(lastCheckDateTime)
        .build();
  }

  @PostConstruct
  @Scheduled(cron = "*/30 * * * * ?")
  public void updateHealthDetails() {
    lastCheckDateTime = LocalDateTime.now();
    currentHealthDetails = healthIndicators.stream()
        .map(indicator -> indicator.getHealthDetail())
        .collect(Collectors.toSet());

  }

  private HealthStatus resolveStatus(Set<HealthDetail> details) {
    if (details.isEmpty()) {
      return HealthStatus.HEALTHY;
    }

    long unhealthyDetails = details.stream()
        .map(detail -> detail.getStatus())
        .filter(status -> status == HealthStatus.UNHEALTHY)
        .count();

    return unhealthyDetails > 0 ? HealthStatus.UNHEALTHY : HealthStatus.HEALTHY;
  }

}
