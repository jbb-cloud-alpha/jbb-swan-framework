package org.jbb.cloud.swan.autoconfigure.orchid;

import java.util.ArrayList;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.jbb.cloud.swan.healthcheck.HealthDetail;
import org.jbb.cloud.swan.healthcheck.HealthDetail.HealthDetailBuilder;
import org.jbb.cloud.swan.healthcheck.HealthIndicator;
import org.jbb.cloud.swan.healthcheck.HealthStatus;
import org.springframework.cloud.config.client.ConfigServicePropertySourceLocator;
import org.springframework.core.env.CompositePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.core.env.PropertySource;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class OrchidServerHealthIndicator implements HealthIndicator {

  private final ConfigServicePropertySourceLocator locator;
  private final Environment environment;

  private long lastAccess = 0;
  private PropertySource<?> cached;

  @Override
  public HealthDetail getHealthDetail() {
    HealthDetailBuilder builder = HealthDetail.builder().name("orchid-server-status");
    PropertySource<?> propertySource = getPropertySource();
    if (propertySource instanceof CompositePropertySource) {
      List<String> sources = new ArrayList<>();
      for (PropertySource<?> ps : ((CompositePropertySource) propertySource).getPropertySources()) {
        sources.add(ps.getName());
      }
      builder.additionalInfo("Known property sources: " + sources);
    } else if (propertySource != null) {
      builder.additionalInfo("Known property sources: " + propertySource);
    } else {
      builder.additionalInfo("None property sources located");
    }
    builder.status(HealthStatus.NOT_APPLICABLE);
    return builder.build();
  }

  private PropertySource<?> getPropertySource() {
    long accessTime = System.currentTimeMillis();
    if (isCacheStale(accessTime)) {
      this.lastAccess = accessTime;
      this.cached = locator.locate(this.environment);
    }
    return this.cached;
  }

  private boolean isCacheStale(long accessTime) {
    if (this.cached == null) {
      return true;
    }
    return (accessTime - this.lastAccess) >= 5000;
  }
}
