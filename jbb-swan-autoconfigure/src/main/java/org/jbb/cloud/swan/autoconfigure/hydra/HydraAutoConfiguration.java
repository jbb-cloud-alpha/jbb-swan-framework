package org.jbb.cloud.swan.autoconfigure.hydra;

import com.netflix.discovery.EurekaClient;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableDiscoveryClient
@ConditionalOnClass(EurekaClient.class)
public class HydraAutoConfiguration {

}
