package org.jbb.cloud.swan.autoconfigure.jpa;

import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

@MappedSuperclass
public abstract class BaseEntity implements Serializable {

  @Id
  @GeneratedValue(generator = "uuid")
  @GenericGenerator(name = "uuid", strategy = "uuid2")
  @Column(name = "uuid", unique = true)
  @Getter
  @Setter
  private String id;

  @Version
  @Column(name = "version")
  @Getter
  @Setter
  private Integer version = 0;

  @Column(name = "create_date_time")
  @Getter
  @Setter
  private LocalDateTime createDateTime;

  @Column(name = "update_date_time")
  @Getter
  @Setter
  private LocalDateTime updateDateTime;

  @PrePersist
  private void onCreate() {
    this.createDateTime = LocalDateTime.now();
    this.updateDateTime = this.createDateTime;
  }

  @PreUpdate
  private void onUpdate() {
    this.updateDateTime = LocalDateTime.now();
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof BaseEntity)) {
      return false;
    }
    BaseEntity that = (BaseEntity) o;
    return id != null ? id.equals(that.id) : that.id == null;

  }

  @Override
  public int hashCode() {
    return id != null ? id.hashCode() : 0;
  }
}