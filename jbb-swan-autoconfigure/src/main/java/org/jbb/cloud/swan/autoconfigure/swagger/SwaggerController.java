package org.jbb.cloud.swan.autoconfigure.swagger;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@Controller
public class SwaggerController {

  @RequestMapping(path = "/swagger", method = RequestMethod.GET)
  public String redirectSwaggerToSwaggerUiHtml() {
    return "redirect:/swagger-ui.html";
  }

}
