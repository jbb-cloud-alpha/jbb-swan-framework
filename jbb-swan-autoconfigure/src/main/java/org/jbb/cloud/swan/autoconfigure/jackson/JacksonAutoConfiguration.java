package org.jbb.cloud.swan.autoconfigure.jackson;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JacksonAutoConfiguration {

  @Bean
  public Jackson2ObjectMapperBuilderCustomizer iso8601dateSerializationCustomizer() {
    return (builder) -> builder
        .featuresToDisable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
        .dateFormat(new ISO8601DateFormat());
  }

  @Bean
  public Module javaTimeModule() {
    return new JavaTimeModule();
  }


}
