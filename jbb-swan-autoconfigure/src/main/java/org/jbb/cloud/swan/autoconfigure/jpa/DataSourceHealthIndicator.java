package org.jbb.cloud.swan.autoconfigure.jpa;

import java.util.List;
import javax.sql.DataSource;
import org.jbb.cloud.swan.healthcheck.HealthDetail;
import org.jbb.cloud.swan.healthcheck.HealthDetail.HealthDetailBuilder;
import org.jbb.cloud.swan.healthcheck.HealthIndicator;
import org.jbb.cloud.swan.healthcheck.HealthStatus;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SingleColumnRowMapper;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class DataSourceHealthIndicator implements HealthIndicator {

  private static final String VALIDATION_QUERY = "SELECT 1";

  private DataSource dataSource;

  private JdbcTemplate jdbcTemplate;

  public DataSourceHealthIndicator(DataSource dataSource) {
    this.dataSource = dataSource;
    this.jdbcTemplate = new JdbcTemplate(dataSource);
  }

  @Override
  public HealthDetail getHealthDetail() {
    HealthDetailBuilder builder = HealthDetail.builder().name("data-source-status");

    String validationQuery = VALIDATION_QUERY;
    if (StringUtils.hasText(validationQuery)) {
      try {
        List<Object> results = this.jdbcTemplate.query(validationQuery,
            new SingleColumnRowMapper());
        Object result = DataAccessUtils.requiredSingleResult(results);
        builder.status(HealthStatus.HEALTHY);
        builder.additionalInfo(
            String.format("Query '%s' returns %s", VALIDATION_QUERY, result)
        );
      } catch (Exception ex) {
        builder.status(HealthStatus.UNHEALTHY);
        builder.additionalInfo("Exception: " + ex);
      }
    }
    return builder.build();
  }


}
