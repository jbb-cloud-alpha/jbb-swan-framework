package org.jbb.cloud.swan.autoconfigure.healthcheck;

import java.util.List;
import org.jbb.cloud.swan.healthcheck.HealthCheckConfiguration;
import org.jbb.cloud.swan.healthcheck.HealthIndicator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@ConditionalOnProperty(prefix = "swan.healthcheck", name = "enabled", matchIfMissing = true)
@Import(HealthCheckConfiguration.class)
@EnableScheduling
public class HealthCheckAutoConfiguration {

  @Autowired
  private List<? extends HealthIndicator> healthIndicators;

  @Value("${spring.application.name}")
  private String appId;

  @Bean
  public HealthCheckService healthCheckService() {
    return new HealthCheckServiceImpl(appId, healthIndicators);
  }

  @Bean
  public HealthCheckController healthCheckController(HealthCheckService healthCheckService) {
    return new HealthCheckController(healthCheckService);
  }


}
