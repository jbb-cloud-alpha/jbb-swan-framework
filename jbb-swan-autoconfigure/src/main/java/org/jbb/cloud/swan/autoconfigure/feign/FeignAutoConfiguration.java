package org.jbb.cloud.swan.autoconfigure.feign;

import feign.Feign;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients("org.jbb.cloud")
@ConditionalOnClass(Feign.class)
public class FeignAutoConfiguration {

}
