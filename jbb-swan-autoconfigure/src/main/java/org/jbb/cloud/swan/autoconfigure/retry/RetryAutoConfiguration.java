package org.jbb.cloud.swan.autoconfigure.retry;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Configuration;
import org.springframework.retry.annotation.EnableRetry;
import org.springframework.retry.support.RetryTemplate;

@Configuration
@EnableRetry
@ConditionalOnClass(RetryTemplate.class)
public class RetryAutoConfiguration {

}
