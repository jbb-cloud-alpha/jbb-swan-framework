package org.jbb.cloud.swan.autoconfigure.healthcheck;

import org.jbb.cloud.swan.healthcheck.HealthCheckInfo;

public interface HealthCheckService {

  HealthCheckInfo getHealthCheckInfo();

}
