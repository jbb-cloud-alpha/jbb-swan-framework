package org.jbb.cloud.swan.autoconfigure.orchid;

import org.jbb.cloud.swan.autoconfigure.healthcheck.HealthCheckService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.cloud.config.client.ConfigServicePropertySourceLocator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
@ConditionalOnBean(ConfigServicePropertySourceLocator.class)
public class OrchidAutoConfiguration {

  @Autowired
  private Environment env;

  @Bean
  @ConditionalOnBean(HealthCheckService.class)
  public OrchidServerHealthIndicator orchidServerHealthIndicator(
      ConfigServicePropertySourceLocator propertySourceLocator) {
    return new OrchidServerHealthIndicator(propertySourceLocator, env);
  }

}
