package org.jbb.cloud.swan.autoconfigure.healthcheck;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.jbb.cloud.swan.healthcheck.HealthStatus;
import org.jbb.cloud.swan.healthcheck.HealthCheckInfo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@Api(value = "/health-check", tags = "Health Check", description = " ")
public class HealthCheckController {

  private final HealthCheckService healthCheckService;

  @ResponseBody
  @RequestMapping(value = "/health-check", method = RequestMethod.GET)
  @ApiOperation(value = "Returns info about app health")
  public ResponseEntity<HealthCheckInfo> getHealthCheckInfo() {
    HealthCheckInfo healthCheckInfo = healthCheckService.getHealthCheckInfo();
    return new ResponseEntity<>(healthCheckInfo, getResponseStatus(healthCheckInfo));
  }

  private HttpStatus getResponseStatus(HealthCheckInfo healthCheckInfo) {
    HealthStatus healthStatus = healthCheckInfo.getStatus();
    return healthStatus == HealthStatus.HEALTHY ? HttpStatus.OK : HttpStatus.SERVICE_UNAVAILABLE;
  }


}
