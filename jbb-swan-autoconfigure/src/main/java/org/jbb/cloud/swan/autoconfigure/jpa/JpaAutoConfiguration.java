package org.jbb.cloud.swan.autoconfigure.jpa;

import javax.persistence.EntityManager;
import javax.sql.DataSource;
import org.jbb.cloud.swan.autoconfigure.healthcheck.HealthCheckService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConditionalOnClass(EntityManager.class)
public class JpaAutoConfiguration {

  @Bean
  @ConditionalOnBean(HealthCheckService.class)
  public DataSourceHealthIndicator dataSourceHealthIndicator(DataSource dataSource) {
    return new DataSourceHealthIndicator(dataSource);
  }

}
